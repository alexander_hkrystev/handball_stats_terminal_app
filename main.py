from threading import Thread, Event
from queue import Queue
from handbal_manager import HandballStatistics, Game

queue = Queue()
data_loaded_event = Event()


class Producer(Thread):

    def run(self):
        with open('input_data/input.dat') as file:
            for line in file:
                line = line.strip()

                if line == 'stop':
                    data_loaded_event.set()
                    return

                input_line = line.split(' | ')

                queue.put(Game(
                    team1_name=input_line[0],
                    team2_name=input_line[1],
                    result1=input_line[2].split(':'),
                    result2=input_line[3].split(':')
                ))


class Consumer(Thread):

    def run(self):

        # Wait until data is added in the queue
        data_loaded_event.wait()

        while not queue.empty():
            game = queue.get()
            handball_statistics.update_stats(game)

        data_loaded_event.clear()


if __name__ == '__main__':

    handball_statistics = HandballStatistics()

    producer = Producer()
    producer.start()

    consumer = Consumer()
    consumer.start()
    consumer.join()

    # Display result in the console
    handball_statistics.display_stats()
