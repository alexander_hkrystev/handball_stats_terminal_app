from dataclasses import dataclass
from typing import List

@dataclass
class Game:
    team1_name: str
    team2_name: str
    result1: List[int]
    result2: List[int]


class TeamsCollection:

    def __init__(self):
        self.items = []

    def append(self, item:"Team"):
        self.items.append(item)

    def __contains__(self, team_name: str) -> bool:
        if team_name in [t.name for t in self.items]:
            return True

    def get(self, team_name: str) -> "Team":

        for team in self.items:
            if team_name == team.name:
                return team

        raise IndexError

    def add_or_select(self, team_name: str) -> "Team":

        for team in self.items:
            if team_name == team.name:
                return team

        # If not exist:
        team = Team(name=team_name)
        self.items.append(team)
        return team

    def __iter__(self) -> "Team":
        for item in self.items:
            yield item

    def __str__(self) -> str:
        return '\n '.join([f'{i.name}: {i.wins}: {i.get_opponent_names()}, ' for i in self.items])


class HandballStatistics:

    def __init__(self):
        self.teams = TeamsCollection()

    def update_stats(self, game: Game):

        # Add teams to collection
        team1 = self.teams.add_or_select(game.team1_name)
        team2 = self.teams.add_or_select(game.team2_name)



        team1.add_opponent(team2)
        team2.add_opponent(team1)

        # results
        team1_points = game.result1[0] + game.result2[1]
        team2_points = game.result1[1] + game.result2[0]

        if team1_points > team2_points:
            team1.increase_wins()
        elif team1_points < team2_points:

            team2.increase_wins()
        elif team1_points == team2_points:

            # Get away soil points
            team1_away_poins = game.result2[1]
            team2_away_points = game.result1[1]

            if team1_away_poins > team2_away_points:
                team1.increase_wins()
            elif team1_away_poins < team2_away_points:
                team2.increase_wins()
            else:
                raise Exception("Cannot determinate winner")

    def display_stats(self):
        """ Sort and display teams data. """
        sorted_by_name = sorted(self.teams, key=lambda team: team.name)
        for team in sorted(sorted_by_name, key=lambda team: team.wins, reverse=True):
            print(f"{team.name}\n"
                  f"- Wins:{team.wins}\n"
                  f"- Opponents: {team.get_opponent_names()}")


class Team:
    def __init__(self, name: str):
        self.name = name
        self.wins = 0
        self.opponents = []

    def increase_wins(self):
        self.wins += 1

    def add_opponent(self, opponent: "Team"):
        if opponent not in self.opponents:
            self.opponents.append(opponent)

    def get_opponent_names(self) -> str:
        """ Return string of sorted by name opponent teams """
        return ', '.join([o.name for o in sorted(self.opponents, key=lambda team: team.name)])

    def __str__(self):
        return self.name
